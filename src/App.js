import React from 'react';
import _ from 'lodash';

// Components
import {
  Header,
  Changes,
  SmartShop,
  MapBlock,
  Sale,
  CurrentWeek,
  Review,
  ReviewsSlider,
  Footer
} from './components';

// Icons
import NavIcon from './assets/icons/navigation.svg';
import DoorIcon from './assets/icons/door.svg';
import ClockIcon from './assets/icons/clock.svg';
import PercentIcon from './assets/icons/percent.svg';
import AnyTimeIcon from './assets/icons/anyTime.svg';
import GreensIcon from './assets/icons/greens.svg';
import { getAllStores } from './api/api';
import { getWeek } from './assets/utils/getWeek';

function App() {
  const [changes, setChanges] = React.useState([
    {
      title: 'Все самое необходимое у вас на виду!',
      description:
        'Мы продумали функциональное зонирование магазинов, удобную и понятную навигацию',
      imageUrl: NavIcon,
      color: 'green'
    },
    {
      title: 'Больше свободного пространства!',
      description: 'Расширили входную зону и проходы между стеллажами',
      imageUrl: DoorIcon,
      color: 'green'
    },
    {
      title: '',
      description:
        'В зонах овощей и пекарни разместили «часы свежести», указывающие на время проверки и выкладки продукции на полку',
      imageUrl: ClockIcon,
      color: 'orange',
      wideDescription: true
    },
    {
      title: 'Самые выгодные предложения всегда под рукой!',
      description:
        'Собрали товары с лучшими ценами на торцах стеллажей, получилась целая «аллея скидок»!',
      imageUrl: PercentIcon,
      color: 'green'
    },
    {
      title: 'Покупки в удобное время!',
      description:
        'Оптимизировали режим работы  так, чтобы магазины всегда были доступны в удобное для вас время',
      imageUrl: AnyTimeIcon,
      color: 'green'
    },
    {
      title: '',
      description: 'Расширили ассортимент отдела «Овощи и фрукты»',
      imageUrl: GreensIcon,
      color: 'orange'
    }
  ]);
  const [stores, setStores] = React.useState([]);
  const [currentWeek, setCurrentWeek] = React.useState(0);

  React.useEffect(() => {
    getAllStores().then(data => {
      const activeStores = Object.values(data).filter(
        item => item.OPENING_DATE !== null
      );
      const groupStoresSize = Object.values(activeStores).length / 3;
      const storesData = _.chunk(Object.values(activeStores), groupStoresSize);
      setStores(storesData);
    });

    setCurrentWeek(getWeek(new Date()));
  }, []);

  return (
    <>
      <div className='App'>
        <Header />
        <Changes changes={changes} />
        <SmartShop />
        <MapBlock stores={stores} />
        <Sale />
        <CurrentWeek stores={stores} currentWeek={currentWeek} />
        <Review />
        {/* <ReviewsSlider /> */}
      </div>
      <Footer />
    </>
  );
}

export default App;
