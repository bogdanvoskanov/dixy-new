import axios from 'axios';

const baseURL = 'https://dixy.ru/api/v1/';

const http = axios.create({
  baseURL
});

export const getAllStores = () => {
  return http.get('STORES').then(res => res.data.ITEMS);
};
