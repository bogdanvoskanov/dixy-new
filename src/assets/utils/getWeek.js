export const getWeek = currentDateTime => {
  const onejan = new Date(currentDateTime.getFullYear(), 0, 1);
  return Math.ceil(
    ((currentDateTime - onejan) / 86400000 + onejan.getDay() + 1) / 7
  );
};
