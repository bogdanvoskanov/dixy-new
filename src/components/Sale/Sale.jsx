import React from 'react';
import Button from '../common/Button/Button';
import styles from './Sale.module.css';
import QrImage from '../../assets/phone/qr-image.png';
import PhoneImage from '../../assets/phone/phone.png';

const Sale = () => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.manual}>
        <h4 className={styles.title}>
          Чтобы получить скидку 5% на покупку в одном из наших новых магазинов
        </h4>
        <p className={styles.subtitle}>
          Cкачайте приложение «‎Дикси» и зарегистрируйтесь в «‎Клубе Друзей
          Дикси»
        </p>
        <p className={styles.remark}>
          {' '}
          * скидка не распространяется на табачную продукцию, алкоголь и товары,
          участвующие в акции
        </p>
        <Button className={styles.button}>Установить приложение</Button>
        <div className={styles.qr}>
          <img src={QrImage} alt='QR Code' />
        </div>
      </div>
      <div className={styles.image}>
        <div className={styles.bg}>
          <img src={PhoneImage} alt='Phone' />
        </div>
      </div>
    </div>
  );
};

export default Sale;
