import React from 'react';
import logoSvg from '../../assets/header/logo.svg';
import headerImg from '../../assets/header/header-image.png';
import styles from './Header.module.css';

const Header = () => {
  return (
    <div className={styles.header}>
      <div className={styles.heading}>
        <h1 className={styles.title}>
          <span>Совершенно новый </span>
          ДИКСИ
        </h1>
        <p className={styles.subtitle}>
          Дикси меняется для вас! Поэтому мы ежедневно работаем над улучшением
          качества обслуживания наших покупателей, открываем новые магазины, а
          также проводим реконструкцию существующих.
        </p>
        <div className={styles.logo}>
          <img src={logoSvg} alt='Logo' />
        </div>
      </div>
      <div className={styles.image}>
        <img src={headerImg} alt='Header' />
      </div>
    </div>
  );
};

export default Header;
