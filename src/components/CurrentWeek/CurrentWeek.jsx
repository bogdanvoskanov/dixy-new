import React from 'react';
import { getWeek } from '../../assets/utils/getWeek';
import styles from './CurrentWeek.module.css';

const CurrentWeek = ({ stores, currentWeek }) => {
  const [currentStores, setCurrentStores] = React.useState([]);

  React.useEffect(() => {
    if (stores.length > 0) {
      const newStores = stores.map(array =>
        array.filter(
          store => getWeek(new Date(store.OPENING_DATE)) === currentWeek
        )
      );
      setCurrentStores(newStores);
    }
  }, [stores]);

  return (
    <>
      {currentStores.flat().length > 0 && (
        <div className={styles.wrapper}>
          <h4 className={styles.title}>Oткрытия текущей недели</h4>

          <div className={styles.stores}>
            {currentStores.map((list, idx) => {
              return (
                <ul key={`${idx}_${Date.now()}`}>
                  {list.map((address, idx) => {
                    return (
                      <li
                        key={`${idx}_${Date.now()}`}
                        className={styles.address}>
                        {address.NAME}
                      </li>
                    );
                  })}
                </ul>
              );
            })}
          </div>
        </div>
      )}
    </>
  );
};

export default CurrentWeek;
