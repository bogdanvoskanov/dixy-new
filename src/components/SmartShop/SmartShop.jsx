import React from 'react';
import styles from './SmartShop.module.css';

// Images
import FirstSmartImg from '../../assets/smart-shop/smart-1.png';
import SecondSmartImg from '../../assets/smart-shop/smart-2.png';

const SmartShop = () => {
  return (
    <div className={styles.shops}>
      <div className={styles.item + ' ' + styles.top}>
        <img src={FirstSmartImg} alt='Shop 1' />
        <p className={styles.description}>
          Обновленные магазины{' '}
          <span>«Дикси» оснащены интеллектуальной системой</span>, которая
          поддерживает <span>комфортные условия</span> внутри торговых точек,
          автоматически управляя освещением, бойлером, электрическими счетчиками
          и счетчиками воды, датчиками движения и вывесками.
        </p>
      </div>
      <div className={styles.item + ' ' + styles.bottom}>
        <p className={styles.description + ' ' + styles.second}>
          <span>«Умный магазин»</span> контролирует изменения температуры и
          настраивает работу кондиционеров в зоне овощей и фруктов для{' '}
          <span>сохранения их свежести</span>. Включает обогрев входной зоны для
          создания «тепловой завесы» в холодную погоду, а также{' '}
          <span>поддерживает идеальные условия</span> для хранения продуктов в
          холодильном оборудовании.
        </p>
        <img src={SecondSmartImg} alt='Shop 2' />
      </div>
    </div>
  );
};

export default SmartShop;
