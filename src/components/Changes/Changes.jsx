import React from 'react';
import styles from './Changes.module.css';
import greenFlag from '../../assets/icons/green-flag.svg';
import orangeFlag from '../../assets/icons/orange-flag.svg';
import cn from 'classnames';

const Changes = ({ changes }) => {
  return (
    <div className={styles.changes}>
      <h2 className={styles.title}>
        Еще больше удовольствия от покупок{' '}
        <strong>в обновленных магазинах Дикси!</strong>
      </h2>
      <div className={styles.cards}>
        {changes.map((card, idx) => {
          return (
            <div key={`${card.title}_${idx}`} className={styles.card}>
              <h5 className={styles.cardTitle}>{card.title}</h5>
              <p
                className={cn(styles.description, {
                  [styles.wide]: card.wideDescription
                })}>
                {card.description}
              </p>
              <div
                className={cn(styles.iconWrapper, {
                  [styles.green]: card.color === 'green',
                  [styles.orange]: card.color === 'orange'
                })}>
                {card.color === 'green' ? (
                  <img
                    src={greenFlag}
                    alt='Green Flag'
                    className={styles.flag}
                  />
                ) : (
                  <img
                    src={orangeFlag}
                    alt='Green Flag'
                    className={styles.flag}
                  />
                )}
                <img
                  className={styles.changesIcon}
                  src={card.imageUrl}
                  alt={card.title}
                />
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Changes;
