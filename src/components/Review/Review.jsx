import React from 'react';

import styles from './Review.module.css';

import LogoImage from '../../assets/review/logo-colored.svg';

import Button from '../common/Button/Button';

const Review = () => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.quote}>
        <h5 className={styles.title}>
          Наши покупатели являются основой нашего развития и совершенствования
        </h5>
        <p className={styles.remark}>
          Оставляя свои предложения и пожелания по товарам и услугам в
          сообществе Дикси в ВК, вы помогаете нам стать лучше!
        </p>
      </div>
      <div className={styles.vk}>
        <div className={styles.wrap}>
          <div className={styles.message}>
            Oставляйте свои предложения в сообщения сообщества Дикси в ВК
          </div>
          <div className={styles.logo}>
            <img src={LogoImage} alt='Logo' />
          </div>
        </div>
        <a
          className={styles.link}
          rel='noreferrer'
          target='_blank'
          href='https://vk.com/dixyclub'>
          <Button className={styles.button}>Перейти в группу ВК</Button>
        </a>
      </div>
    </div>
  );
};

export default Review;
