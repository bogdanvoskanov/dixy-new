export { default as Header } from './Header/Header';
export { default as Changes } from './Changes/Changes';
export { default as SmartShop } from './SmartShop/SmartShop';
export { default as MapBlock } from './Map/MapBlock';
export { default as Sale } from './Sale/Sale';
export { default as CurrentWeek } from './CurrentWeek/CurrentWeek';
export { default as Review } from './Review/Review';
export { default as ReviewsSlider } from './ReviewsSlider/ReviewsSlider';
export { default as Footer } from './Footer/Footer';
