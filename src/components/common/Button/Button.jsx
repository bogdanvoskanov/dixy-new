import React from 'react';
import styles from './Button.module.css';
import cn from 'classnames';

const Button = ({ className, children }) => {
  return <button className={cn(className, styles.button)}>{children}</button>;
};

export default Button;
