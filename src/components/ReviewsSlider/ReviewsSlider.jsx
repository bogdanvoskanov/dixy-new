import React from 'react';
import { SliderData } from './SliderData';
import StarIcon from '../../assets/icons/star-icon.svg';
import Slider from 'react-slick';

import styles from './ReviewsSlider.module.css';

const ReviewsSlider = () => {
  const settingsDesktop = {
    dots: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    infinite: false
  };

  const settingsMobile = {
    dots: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: false,
    arrows: false
  };

  const windowSize = window.innerWidth;

  return (
    <div className={styles.wrapper}>
      <h4 className={styles.title}>Отзывы</h4>
      {windowSize > 768 ? (
        <Slider {...settingsDesktop}>
          {SliderData.map((slide, idx) => {
            return (
              <div key={idx} className={styles.slide}>
                <h6 className={styles.author}>{slide.author}</h6>
                <div className={styles.row}>
                  <div className={styles.rating}>
                    {Array(slide.rating)
                      .fill(StarIcon)
                      .map((icon, idx) => {
                        return <img key={idx} src={icon} alt='Star' />;
                      })}
                  </div>
                  <p className={styles.date}>{slide.date}</p>
                </div>
                <span className={styles.heading}>{slide.title}</span>
                <p className={styles.body}>{slide.body}</p>
              </div>
            );
          })}
        </Slider>
      ) : (
        <Slider {...settingsMobile}>
          {SliderData.map((slide, idx) => {
            return (
              <div key={idx} className={styles.slide}>
                <h6 className={styles.author}>{slide.author}</h6>
                <div className={styles.row}>
                  <div className={styles.rating}>
                    {Array(slide.rating)
                      .fill(StarIcon)
                      .map((icon, idx) => {
                        return <img key={idx} src={icon} alt='Star' />;
                      })}
                  </div>
                  <p className={styles.date}>{slide.date}</p>
                </div>
                <span className={styles.heading}>{slide.title}</span>
                <p className={styles.body}>{slide.body}</p>
              </div>
            );
          })}
        </Slider>
      )}
    </div>
  );
};

export default ReviewsSlider;
