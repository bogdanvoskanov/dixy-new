import React from 'react';
import styles from './MapBlock.module.css';
import pinDiscount from '../../assets/map/discount.svg';
import logo from '../../assets/map/logo-dixy.png';
import cluster from '../../assets/map/cluster.png';
import placemark from '../../assets/map/placemark.png';

class MapBlock extends React.Component {
  componentDidMount() {
    window.ymaps.ready(this.setInit);
    console.log(this.props.stores.flat());
  }

  setInit = () => {
    setTimeout(this.init, 100);
  };

  init = () => {
    let zoomControl = new window.ymaps.control.ZoomControl({
      options: {
        position: {
          right: 50,
          top: 50
        },
        size: 'large'
      }
    });

    this.myMap = new window.ymaps.Map('map', {
      center: [55.753215, 37.622504],
      zoom: 11,
      controls: [zoomControl]
    });

    let objectManager = new window.ymaps.ObjectManager({
      clusterize: true,
      gridSize: 64,
      clusterDisableClickZoom: false
    });

    let objectManager2 = new window.ymaps.ObjectManager({
      clusterize: true,
      gridSize: 64,
      clusterDisableClickZoom: false
    });
    let MyIconContentLayout = window.ymaps.templateLayoutFactory.createClass(
      '<div style="color: #fff; font-weight:bold;width:46px;vertical-align:middle;line-height:46px;">$[properties.iconContent]</div>'
    );
    objectManager.objects.options.set({
      iconLayout: 'default#image',
      iconImageHref: pinDiscount,
      iconImageSize: [46, 56]
    });

    objectManager2.objects.options.set({
      iconLayout: 'default#image',
      iconImageHref: placemark,
      iconImageSize: [46, 56]
    });

    objectManager.clusters.options.set({
      clusterIconLayout: 'default#imageWithContent',
      clusterIconImageHref: cluster,
      clusterIconImageSize: [46, 46],
      clusterIconImageOffset: [-23, -23],
      clusterIconContentLayout: MyIconContentLayout,
      clusterHideIconOnBalloonOpen: false,
      clusterDisableClickZoom: false
    });

    objectManager2.clusters.options.set({
      clusterIconLayout: 'default#imageWithContent',
      clusterIconImageHref: cluster,
      clusterIconImageSize: [46, 46],
      clusterIconImageOffset: [-23, -23],
      clusterIconContentLayout: MyIconContentLayout,
      clusterHideIconOnBalloonOpen: false,
      clusterDisableClickZoom: false
    });

    this.myMap.geoObjects.add(objectManager);
    this.myMap.geoObjects.add(objectManager2);
    this.myMap.behaviors.disable('scrollZoom');

    let openD = this.props.stores.flat().filter(item => {
      let spl = item.DISCOUNT_DATE.split('.');
      return new Date(spl[2], spl[1] - 1, spl[0]) > new Date();
    });

    let notOpenD = this.props.stores.flat().filter(item => {
      let spl = item.DISCOUNT_DATE.split('.');
      return new Date(spl[2], spl[1] - 1, spl[0]) < new Date();
    });

    let resultingObjects = openD.map(async (item, index) => {
      return {
        type: 'Feature',
        id: index,
        geometry: {
          type: 'Point',
          coordinates: item.COORDS.split(',')
        },
        properties: {
          balloonContentHeader: `<div class="row align-items-center shopHint">
          <img src=${logo} />Мой магазин ДИКСИ</div>`,
          balloonContentFooter: item.SCHEDULE,
          balloonContentBody: item.NAME
        }
      };
    });

    let resultingObjects2 = notOpenD.map(async (item, index) => {
      return {
        type: 'Feature',
        id: index,
        geometry: {
          type: 'Point',
          coordinates: item.COORDS.split(',')
        },
        properties: {
          balloonContentHeader: `<div class="row align-items-center shopHint">
          <img src=${logo} />Мой магазин ДИКСИ</div>`,
          balloonContentFooter: item.SCHEDULE,
          balloonContentBody: item.NAME
        }
      };
    });

    let resultingData;
    Promise.all(resultingObjects).then(completed => {
      resultingData = {
        type: 'FeatureCollection',
        features: completed
      };
      objectManager.add(resultingData);
    });

    let resultingData2;
    Promise.all(resultingObjects2).then(completed => {
      resultingData2 = {
        type: 'FeatureCollection',
        features: completed
      };
      objectManager2.add(resultingData2);
    });
  };

  render() {
    return (
      <div className={styles.wrapper}>
        <div id='map' className={styles.map}></div>
      </div>
    );
  }
}

export default MapBlock;
